<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "Name : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 4
echo "cold blooded : $sheep->cold_blooded <br>"; // "no"
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

 $sungokong = new Ape("kera sakti");
 echo "Name : $sungokong->name <br>"; // "kera sakit"
 echo "legs : $sungokong->legs <br>"; // 2
 echo "cold blooded : $sungokong->cold_blooded <br>"; // "no"
 echo "Yell :";
 echo  $sungokong->yell(); // "Auooo"
 echo "<br>";
 echo "<br>";

 $kodok = new Frog("buduk");
 echo "Name : $kodok->name <br>"; // "buduk"
 echo "legs : $kodok->legs <br>"; // 4
 echo "cold blooded : $kodok->cold_blooded <br>"; // "no"
 echo $kodok->jump(); // "hop hop"

?>
